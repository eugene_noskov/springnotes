package org.eugene;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/*
Аспекты не могут цепляться на другие аспекты
 */
@Aspect
@Component
@Slf4j
public class LoginAspect {
/*

    //1 способ :: через эвайсы

    @Before("execution(public String org.eugene.AppConfig.HelloWorld.sayHello())")
    @Before("execution(public String org.eugene.AppConfig.*.sayHello())") //в любом классе пакета
    @Before("execution(public String sayHello())")
    @Before("execution(* get*())") //Все гет методы с любым  возвращаемым типом (*), любой закрытости, без параметров
    @Before("execution(* get*(*))") //..с любыми входящими параметром
    @Before("execution(* get*(..))") //..с любыми входящими параметром или без него
    @Before("execution(* *.sayHello(..))")
    @Before("execution(* sayHello() && * sayGoodbye())") //комбинирование
    public void logBeforeAdvice() {
        String simpleName = jointPoint.getTarget().getClass().getSimpleName();
        String name = jointPoint.getSignature().getName();
        log.info("Hello from ASPECTS: class {} method {}", simpleName, name);
    }

    @Before("args(name)") //параметр должен назваться name
    public void logBeforeAdvice(String name)


*/

/*
    //2 способ :: через пойнткаты
*/

//    @Pointcut("execution(public String sayHello())")
//    public void sayHelloOrNot(){
//
//    }
//
//    @Before("sayHelloOrNot()")
//    public void helloAdviceOne(){
//        System.out.println("Hello One Time!");
//    }
//
//    @Before("sayHelloOrNot()")
////    @Before("within(org.eugene.AppConfig)") //все методы класса
////    @Before("within(org.eugene.*)") //все методы классов пакета
////    @Before("within(org.eugene..*)") //все методы классов пакета и подпакетов
////    @Before("within(org.eugene.Circle.*)") //подклассы класов
//    public void helloAdviceTwo(){
//        System.out.println("Hello Two Time!");
//    }


//    @Pointcut("args(..)") //прокси

//    @Before("args(String)") //методы, принимающие строку в качестве аргумента, параметр в эвайсе наверно не нужен
//    @Before("execution(* setName())") //параметр должен назваться name
    @Before("args(var)") //параметр должен назваться name
    public void setNameAdvice(String var){
        log.warn("Circle name is {}", var);
    }



    //JOINT POINTS - информация о методе, на котором сработало
    //    @Before("execution(* sayHello()") //комбинирование
    public void logBeforeAdvice(JoinPoint jointPoint) {
        jointPoint.getTarget(); //Объкт на котором вызвали метод
    }





//    //2 с поиткатами. Шаблон какие методы должны попадать
//    @Pointcut("execution(* *.sayHello(..))")
//    private void allLogEventMethods() {
//
//    }
////    Выполнится непосредственно до метода
//    @Before("allLogEventMethods()")
//    public void logBeforeAdvice(JoinPoint jointPoint) {
//        String simpleName = jointPoint.getTarget().getClass().getSimpleName();
//        String name = jointPoint.getSignature().getName();
//        log.info("Hello from ASPECTS: class {} method {}", simpleName, name);
//
//    }
//
//    @AfterReturning(pointcut = "allLogEventMethods()", returning = "retVal")
//    public void logAfterAdvice(Object retVal){
//        log.info("Returned value is {}", retVal);
//    }
//
//    @AfterThrowing(pointcut = "allLogEventMethods()", throwing = "ex")
//    public void logAfterThrowAdvice(Throwable ex){
//        log.warn("Thrown: {}", ex);
//    }
//
//    //Выполнится вместо метода
////    @Around("allLogEventMethods()")
////    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
////        joinPoint.proceed();
////    }
//
//
//    @Around("allLogEventMethods() && args(evt)")
//    public void aroundAdvice(ProceedingJoinPoint joinPoint, Object evt) throws Throwable {
//        //joinPoint.proceed(new Object[] {evt});
//        //joinPoint.proceed(new Object[]{args});
//        System.out.println("Yo-yo-yo");
//    }
}

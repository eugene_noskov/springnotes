package org.eugene;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan
public class AppConfig {
    @Bean(name="myBean")
    public HelloWorld getHelloWorld(){
        return new HelloWorld();
    }

    public class HelloWorld {
        public String sayHello() {
            return "My name is, my name is...";
        }

        public String sayGoodbye(){
            return sayGoodbye();
        }
    }
}

package org.eugene;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Circle {
    private String name;
    private int radius;
}

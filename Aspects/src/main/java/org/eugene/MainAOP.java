package org.eugene;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
@Slf4j
public class MainAOP {

    public static void main(String[] args) {
        @SuppressWarnings("resource")
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        //AppConfig.HelloWorld helloWorldBean = context.getBean("myBean", AppConfig.HelloWorld.class);
        //log.warn(helloWorldBean.sayHello());

        Circle circle = context.getBean("circle", Circle.class);
        circle.setName("Orange");
    }
}

package org.eugene.beans.loggers;

import org.eugene.beans.beans.Event;

public interface EventLogger {
    public void logEvent(Event event);
}

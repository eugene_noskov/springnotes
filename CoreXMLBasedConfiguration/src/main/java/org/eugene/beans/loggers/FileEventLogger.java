package org.eugene.beans.loggers;

import org.apache.commons.io.FileUtils;
import org.eugene.beans.beans.Event;

import java.io.File;
import java.io.IOException;

public class FileEventLogger implements EventLogger {

    String fileName;
    private File file;

    public FileEventLogger(String fileName) {
        this.fileName = fileName;
    }

    //В методе иниицализации не может быть аргументов
    //Модификатор доступа любой
    //может возвращать что-угодно и выбрасывать эксцепшны
    public void init() throws IOException {
        this.file = new File(fileName);
        if(!this.file.canWrite()){
            //throw new IOException("Read file is blocked!");
        }
    }

    @Override
    public void logEvent(Event event) {
        System.out.println(event + " filename " + fileName);
        try {
            FileUtils.writeStringToFile(file, event.toString(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

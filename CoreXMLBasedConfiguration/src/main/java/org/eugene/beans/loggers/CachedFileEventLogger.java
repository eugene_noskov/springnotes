package org.eugene.beans.loggers;

import org.eugene.beans.beans.Event;

import java.io.IOException;
import java.util.List;

public class CachedFileEventLogger extends FileEventLogger {

    Integer cacheSize;
    List<Event> cache;

    public CachedFileEventLogger(String fileName) {
        super(fileName);
    }

    @Override
    public void logEvent(Event event) {
        cache.add(event);
        if (cacheSize >= cacheSize){
            writeEventsFromCache();
            cache.clear();
        }
    }

    private void writeEventsFromCache() {
        for (Event event : cache) {
            System.out.println(event);
        }
    }

    public void setCacheSize(Integer cacheSize) {
        this.cacheSize = cacheSize;
    }

    public void destroy(){
        if(!cache.isEmpty()){
            writeEventsFromCache();
        }
    }
}

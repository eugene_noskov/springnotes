package org.eugene.beans.loggers;

import org.eugene.beans.beans.Event;

public class ConsoleEventLogger implements EventLogger {
    @Override
    public void logEvent(Event event) {
        System.out.println(event);
    }

}

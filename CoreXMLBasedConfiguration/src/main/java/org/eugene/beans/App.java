package org.eugene.beans;

import org.eugene.beans.beans.Client;
import org.eugene.beans.loggers.ConsoleEventLogger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    private Client client;
    private ConsoleEventLogger eventLogger;

    public App(Client client, ConsoleEventLogger eventLogger) {
        this.client = client;
        this.eventLogger = eventLogger;
    }

    public static void main(String[] args) {

        //ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
        //Для дестрой методов
        ConfigurableApplicationContext parentContext = new ClassPathXmlApplicationContext("spring.xml");

        //Здесь должны быть доступны все бины родительсткого контекста
        // не работает???
        //ApplicationContext childContext = new ClassPathXmlApplicationContext("spring.xml", parentContext);

        App app = (App) parentContext.getBean("app");

        app.logEvent("Some event 1");
        app.logEvent("Some event 2");

        //Для дестрой методов и этот метод вызовет шатдаунхук сам,
        //при завершении
        //ctx.close();
    }

    private void logEvent(String msg) {
        String message = msg.replaceAll(client.getId(), client.getFullName());
        //eventLogger.logEvent(message);
    }
}

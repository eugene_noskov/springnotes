package org.eugene.beans.beans;

import java.text.DateFormat;
import java.util.Date;

public class Event {
    private final DateFormat df;
    int id;
    String msg;
    Date date;

    public Event(Date date, DateFormat df) {
        this.date = date;
        this.df = df;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", msg='" + msg + '\'' +
                ", date=" + df.format(date) +
                '}';
    }
}


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class App {


    public static void main(String[] args) throws IOException {

        ConfigurableApplicationContext parentContext = new AnnotationConfigApplicationContext(Config.class);
        new App();

    }


    //https://www.baeldung.com/jackson-object-mapper-tutorial
    //https://www.baeldung.com/jackson-annotations

    public App() throws IOException {

        fromJSON();
        customization();
        serialize();
        deserialize();
        handlingDateFormat();
        handlingCollections();
    }

    public void toJson() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Car car = new Car("yellow", "renault");

        File file = new File("temp/car.json");
        objectMapper.writeValue(file, car);

        String jsonStr = objectMapper.writeValueAsString(car);
    }

    public void fromJSON() throws IOException {
        String json = "{ \"color\" : \"Black\", \"type\" : \"FIAT\" }";

        ObjectMapper objectMapper = new ObjectMapper();

        Car carFromString = objectMapper.readValue(json, Car.class);
        Car carFromJSON = objectMapper.readValue(new File("temp/car.json"), Car.class);

        String jsonCarArray = "[{ \"color\" : \"Black\", \"type\" : \"BMW\" }, { \"color\" : \"Red\", \"type\" : \"FIAT\" }]";
        List<Car> listCar = objectMapper.readValue(jsonCarArray, new TypeReference<List<Car>>(){});

        json = "{ \"color\" : \"Black\", \"type\" : \"BMW\" }";
        Map<String, Object> map = objectMapper.readValue(json, new TypeReference<Map<String,Object>>(){});

    }

    public void readToJsonNode() throws IOException {
        String json = "{ \"color\" : \"Black\", \"type\" : \"FIAT\" }";

        ObjectMapper objectMapper = new ObjectMapper();

        //JSONNode
        JsonNode jsonNode = objectMapper.readTree(json);
        String color = jsonNode.get("color").asText();
    }

    public void customization() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        String jsonString = "{ \"color\" : \"Black\", \"type\" : \"Fiat\" , \"year\" : \"1970\"}";

        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Car car = objectMapper.readValue(jsonString, Car.class);

        JsonNode jsonNodeRoot = objectMapper.readTree(jsonString);
        JsonNode jsonNodeYear = jsonNodeRoot.get("year");
        String year = jsonNodeYear.asText();
    }

    public void serialize() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        SimpleModule module =
                new SimpleModule("CustomCarSerializer", new Version(1, 0, 0, null, null, null));
        module.addSerializer(Car.class, new CustomCarSerializer());

        mapper.registerModule(module);

        Car car = new Car("yellow", "renault");
        String carJson = mapper.writeValueAsString(car);
    }

    public void deserialize() throws IOException {
        String json = "{ \"color\" : \"Black\", \"type\" : \"BMW\" }";

        ObjectMapper mapper = new ObjectMapper();

        SimpleModule module =
                new SimpleModule("CustomCarDeserializer", new Version(1, 0, 0, null, null, null));
        module.addDeserializer(Car.class, new CustomCarDeserializer());
        mapper.registerModule(module);

        Car car = mapper.readValue(json, Car.class);
    }

    public void handlingDateFormat() throws JsonProcessingException {
        Car car = new Car("yellow", "renault");
        Request request = new Request(car, new Date());

        ObjectMapper objectMapper = new ObjectMapper();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm a z");
        objectMapper.setDateFormat(df);
        String carAsString = objectMapper.writeValueAsString(request);
        // output: {"car":{"color":"yellow","type":"renault"},"datePurchased":"2016-07-03 11:43 AM CEST"}

    }

    public void handlingCollections() throws IOException {
        String jsonCarArray =
                "[{ \"color\" : \"Black\", \"type\" : \"BMW\" }, { \"color\" : \"Red\", \"type\" : \"FIAT\" }]";

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
        Car[] cars = objectMapper.readValue(jsonCarArray, Car[].class);
        // print cars

        List<Car> listCar = objectMapper.readValue(jsonCarArray, new TypeReference<List<Car>>(){});
        // print cars
    }

}

import components.SomeComp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
//@PropertySource({"classpath:foo.properties", "classpath:bar.properties"})
@ComponentScan(basePackages = "components")
public class Config {
    @Autowired
    private Environment env;


    @Bean
    public String someBean(){
        String  envStr = env.getProperty("bar.jdbc.url");
        return "";
    }

}

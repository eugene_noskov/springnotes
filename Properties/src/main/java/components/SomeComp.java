package components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class SomeComp {

    @Value( "${foo.jdbc.url}" )
    public String fooJdbcUrl;

    @Value( "${bar.jdbc.url}" )
    public String barJdbcUrl;

    @Value( "${my.greeting}" )
    public String profileName;

    @PostConstruct
    public void init(){
        //String  env = env.getProperty("jdbc.url");
        int a = 1;
    }
}
